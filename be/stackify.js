/**
 * Stackify Node APM Configuration
 */
exports.config = {
  /**
   * Your application name.
   */
  application_name: 'Node Application',
  /**
   * Your environment name.
   */
  environment_name: 'Production'
}
