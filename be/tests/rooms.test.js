require("dotenv").config();
const mongoose = require("mongoose");
const request = require("supertest");
const { faker } = require("@faker-js/faker");
const app = require("../src/app");
const {
  afterEach,
  describe,
  expect,
  it,
  beforeEach,
  afterAll,
  beforeAll,
} = require("@jest/globals");
const {
  randomIntFromInterval,
  getNRandomFeatures,
  checkStatusCode,
} = require("./utils/functions");

let db = null;
const name = faker.name.fullName();
const email = faker.internet.email();
const password = faker.internet.password();

let newUser,
  newToken,
  newRoom = null;

beforeEach(async () => {
  db = await mongoose.createConnection(process.env.DB_TEST);
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => {
    console.log("MongoDB connected");
  });
});

afterEach(async () => {
  await db.close();
});

afterAll(async () => {
  if (db) {
    await db.close();
    mongoose.connection.close();
    mongoose.disconnect();
  }
});

describe("Room controller", () => {
  it("should register a user", async () => {
    await request(app)
      .post("/users")
      .send({
        user: {
          name: name,
          email: email,
          password: password,
        },
      })
      .then((response) => {
        newUser = response.body.success.data;
      });
  });

  it("should log in and save the token", async () => {
    await request(app)
      .post("/users/login")
      .send({
        user: {
          email: email,
          password: password,
        },
      })
      .then((response2) => {
        newToken = response2.body.success.data.token;
      });
  });

  it("a user should create a room", async () => {
    const res = await request(app)
      .post("/rooms")
      .set("Authorization", `Token ${newToken}`)
      .send({
        room: {
          name: `${randomIntFromInterval(
            1,
            9
          )} ${faker.address.cardinalDirection(false)}`,
          created_by: newUser.id,
          location: faker.address.city(),
          features: getNRandomFeatures(3),
        },
      });
    
    newRoom = res.body.success.data.token;
    checkStatusCode(res, 201);
    expect(res.statusCode).toBe(201);
    expect(res.body.success.message).toBe("Room successfully created!");
  });
});
