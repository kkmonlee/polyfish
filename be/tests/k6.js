import http from "k6/http";
import { check } from "k6";

const username = "atulanand_@outlook.com";
const password = "Password123";

export default function () {
  const url = "https://api.polyfish.systems/users/login";
  const payload = JSON.stringify({
    user: {
      email: username,
      password: password,
    },
  });
  const params = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  let res = http.post(url, payload, params);
  const apiResponse = JSON.parse(res.body).success.data.email;

  // Verify response (checking the echoed data from the httpbin.test.k6.io
  // basic auth test API endpoint)
  check(res, {
    'status is 200': (r) => r.status === 200,
    'is correct email': (r) => apiResponse === username,
  });
}
