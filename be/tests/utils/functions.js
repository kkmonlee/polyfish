// https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript
const { FEATURE_TYPE_ARRAY } = require("./constants");
const randomIntFromInterval = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

// https://stackoverflow.com/questions/19269545/how-to-get-a-number-of-random-elements-from-an-array
const getNRandomFeatures = (n) => {
  return FEATURE_TYPE_ARRAY.sort(() => 0.5 - Math.random()).slice(0, n);
};

// https://stackoverflow.com/questions/67709256/how-to-print-the-request-logs-like-request-url-request-body-request-queryparam
const checkStatusCode = (res, expectedStatus) => {
  if (res.status === expectedStatus) {
    return res;
  }
  const error = res.error;
  const reqData = JSON.parse(JSON.stringify(res)).req;
  throw new Error(`
  request-method  : ${JSON.stringify(reqData.method)}
  request-url     : ${JSON.stringify(reqData.url)}
  request-data    : ${JSON.stringify(reqData.data)}
  request-headers : ${JSON.stringify(reqData.headers)}
  response-status  : ${JSON.stringify(res.status)}
  response-body    : ${JSON.stringify(res.body)}
  `);
};

module.exports = {
  checkStatusCode,
  randomIntFromInterval,
  getNRandomFeatures,
};
