const FEATURE_TYPE_ARRAY = [
  "Ethernet",
  "Power Plug",
  "Group Study",
  "Silent Study",
  "Video Conferencing",
  "Television",
  "Computer",
];

module.exports = {
  FEATURE_TYPE_ARRAY,
};