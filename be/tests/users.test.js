require("dotenv").config();

const mongoose = require("mongoose");
const request = require("supertest");
const { faker } = require("@faker-js/faker");
const app = require("../src/app");
const {
  afterEach,
  describe,
  expect,
  it,
  beforeEach,
  afterAll,
} = require("@jest/globals");
const { checkStatusCode } = require("./utils/functions");
const User = require("../src/models/User");
const { ObjectId } = require("mongodb");

let db = null;
beforeEach(async () => {
  db = mongoose.createConnection(process.env.DB_TEST);
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => {
    console.log("MongoDB connected");
  });
});

afterEach(async () => {
  await db.close();
});

describe("User model", () => {
  it("should run User.save()", async () => {
    const name = faker.name.fullName();
    const email = faker.internet.email();
    const password = faker.internet.password();

    const user = new User();
    user.email = email;
    user.name = name;
    user.setPassword(password);

    expect(user).toBeInstanceOf(User);

    await User.init().then(() => user.save());

    const dbUser = await User.findOne({ email: email });
    expect(dbUser._id).toBeInstanceOf(ObjectId);
    expect(dbUser.name).toBe(name);
    expect(dbUser.email).toBe(email.toLowerCase());
  });
})
describe("User controller", () => {
  const name = faker.name.fullName();
  const email = faker.internet.email();
  const password = faker.internet.password();

  let newUser = null;
  let newToken = null;

  describe("POST /users", () => {
    it("should register a user", async () => {
      const res = await request(app)
        .post("/users")
        .send({
          user: {
            name: name,
            email: email,
            password: password,
          },
        });
      newUser = res.body.success.data;
      checkStatusCode(res, 201);
      expect(res.statusCode).toBe(201);
      expect(res.body.success.message).toBe("User successfully created!");
    });


  });

  describe("POST /users/login", () => {
    it("should log in", async () => {
      const res = await request(app)
        .post("/users/login")
        .send({
          user: {
            email: email,
            password: password,
          },
        });
      newToken = res.body.success.data.token;
      checkStatusCode(res, 200);
      expect(res.statusCode).toBe(200);
      expect(res.body.success.message).toBe("Logged in successfully!");
    });
  });

  describe("GET /user", () => {
    it("should retrieve a user", async () => {
      const res = await request(app)
        .get("/user")
        .set("Authorization", `Token ${newToken}`)
        .send({ id: newUser.id });
      checkStatusCode(res, 200);
      expect(res.statusCode).toBe(200);
      expect(res.body.success.message).toBe("User retrieved successfully!");
    });
  });

  describe("PUT /user", () => {
    it("should update the user's name and password", async () => {
      const res = await request(app)
        .put("/user")
        .set("Authorization", `Token ${newToken}`)
        .send({
          user: {
            id: newUser.id,
            name: faker.name.fullName(),
            password: faker.internet.password(),
          },
        });
      checkStatusCode(res, 200);
      expect(res.statusCode).toBe(200);
      expect(res.body.success.data.name).not.toEqual(newUser.name);
      expect(res.body.success.message).toBe("User updated successfully!");
      // cannot test for password since it tokenizes
    });
  });
});

afterAll(() => {
  mongoose.connection.close();
  mongoose.disconnect();
});
