const apiRouter = require("express").Router();
const cors = require("cors");

apiRouter.use(cors());
apiRouter.options("*", cors());

apiRouter.use("/", require("./users"));

apiRouter.use("/", require("./rooms"));

apiRouter.use("/", require("./tables"));

apiRouter.use("/", require("./seats"));

module.exports = apiRouter;
