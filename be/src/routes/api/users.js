const userRouter = require("express").Router();
const passport = require("passport");
const auth = require("../auth");

const User = require("../../models/User");
const {
  sendErrorResponse,
  sendSuccessResponse,
} = require("../../utils/network");

// User registration
// TODO: next() usage
userRouter.post("/users", (req, res, next) => {
  if (
    typeof req.body?.user?.email === "undefined" ||
    typeof req.body?.user?.name === "undefined" ||
    typeof req.body?.user?.password === "undefined"
  ) {
    return sendErrorResponse(
      res,
      406,
      "Please enter your email, name, and password!"
    );
  }

  const user = new User();
  user.email = req.body.user.email; // Email field
  user.name = req.body.user.name; // Name field
  user.setPassword(req.body.user.password); // Password field

  // Check if the registration is by an admin
  if (req.body?.user?.role === "admin") {
    user.role = req.body.user.role;
  }

  User.init().then(() => {
    user
      .save()
      .then(() => {
        return sendSuccessResponse(
          res,
          201,
          "User successfully created!",
          user.toAdminJSON()
        );
      })
      .catch((err) => {
        console.error(err);
        if (err.name === "ValidationError") {
          return sendErrorResponse(res, 422, null, err);
        }
      });
  });
});

// User update
userRouter.put("/user", auth.required, (req, res, next) => {
  User.findById(req.body.user.id)
    .then((user) => {
      if (!user) return sendErrorResponse(res, 401, "User ID is invalid!");

      if (typeof req.body.user.name !== "undefined") {
        user.name = req.body.user.name;
      }
      if (typeof req.body.user.email !== "undefined") {
        user.email = req.body.user.email;
      }
      if (typeof req.body.user.password !== "undefined") {
        user.setPassword(req.body.user.password);
      }

      return user.save().then(() => {
        return sendSuccessResponse(
          res,
          200,
          "User updated successfully!",
          user.toAdminJSON()
        );
      });
    })
    .catch(next);
});

// User login
userRouter.post("/users/login", (req, res, next) => {
  if (!req.body.user.email) {
    return sendErrorResponse(res, 422, "Email cannot be empty!");
  }
  if (!req.body.user.password) {
    return sendErrorResponse(res, 422, "Password cannot be empty!");
  }

  // Local strategy configured in config/passport.js
  // session: false to prevent Passport from serializing user into the session
  // callback -> done() in config/passport.js
  passport.authenticate(
    "local-login",
    { session: false },
    (err, user, info) => {
      if (err) return next(err);

      if (user) {
        user.token = user.generateToken();
        return sendSuccessResponse(
          res,
          200,
          "Logged in successfully!",
          user.toAdminJSON()
        );
      } else {
        return sendErrorResponse(res, 422, "Email or password is incorrect!");
      }
    }
  )(req, res, next);
});

// User get by ID
userRouter.get("/user", auth.required, (req, res, next) => {
  User.findById(req.body.id)
    .then((user) => {
      // edge case: user's token is deleted from the database
      if (!user) return sendErrorResponse(res, 401, "User ID is invalid!");

      return sendSuccessResponse(
        res,
        200,
        "User retrieved successfully!",
        user.toAdminJSON()
      );
    })
    .catch(next);
});

// Delete user by ID
userRouter.delete("/user/:userId", auth.required, async (req, res) => {
  if (req.params.userId) {
    const user = await User.findById(req.params.userId);
    if (user) {
      // req.params.userId
      await User.deleteOne({ _id: req.params.userId });
      return sendSuccessResponse(res, 200, "User successfully deleted");
    } else {
      return sendErrorResponse(res, 404, "This user does not exist");
    }
  } else {
    return sendErrorResponse(res, 402, "User ID is malformed");
  }
});

module.exports = userRouter;
