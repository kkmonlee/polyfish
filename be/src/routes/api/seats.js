const cors = require("cors");
const auth = require("../auth");
const {
  sendErrorResponse,
  sendSuccessResponse,
} = require("../../utils/network");
const Table = require("../../models/Table");
const Seat = require("../../models/Seat");

const seatRouter = require("express").Router();
seatRouter.use(cors());
seatRouter.options("*", cors());

seatRouter.post("/seats", auth.required, async (req, res) => {
  if (typeof req.body?.seat?.belongs_to === "undefined") {
    return sendErrorResponse(res, 406, "Please send the correct parameters.");
  }

  const filter = { _id: req.body.seat.belongs_to };
  let parentTable = await Table.findOne(filter);

  if (parentTable) {
    const resultSet = await Seat.find({ belongs_to: parentTable._id });
    // Check if parent table already has 6 seats
    if (resultSet.length + 1 <= 6) {
      let seat = new Seat();
      seat.occupied = false;
      seat.belongs_to = parentTable._id;

      await seat.save().then((response) => {
        return sendSuccessResponse(
          res,
          201,
          "Seat added successfully!",
          response
        );
      });
    } else {
      return sendErrorResponse(
        res,
        400,
        "You cannot add more than 6 seats to a table!"
      );
    }
  } else {
    return sendErrorResponse(res, 404, "This table does not exist!");
  }
});

seatRouter.get("/seats/:seatId", auth.required, async (req, res) => {
  if (req.params.seatId) {
    const resultSet = await Seat.find({ belongs_to: req.params.seatId });

    if (resultSet) {
      sendSuccessResponse(
        res,
        200,
        `${
          resultSet.length > 0 ? resultSet.length : "No"
        } seats found for this table.`,
        resultSet
      );
    }
  } else {
    return sendErrorResponse(res, 404, "The table ID is invalid!");
  }
});

seatRouter.put(
  "/update-seat-occupied/:seatId",
  auth.required,
  async (req, res) => {
    if (
      typeof req.params.seatId !== "undefined" &&
      typeof req.body.seat.occupied !== "undefined"
    ) {
      const filter = { _id: req.params.seatId };
      await Seat.findOneAndUpdate(
        filter,
        { $set: { occupied: req.body.seat.occupied } },
        { returnDocument: "after" }
      )
        .then((doc) => {
          sendSuccessResponse(
            res,
            200,
            "Seat successfully updated",
            doc.toJSON()
          );
        })
        .catch((e) => {
          sendErrorResponse(res, 400, "Something went wrong!");
          console.log(e);
        });
    } else {
      return sendErrorResponse(res, 406, "Please send the correct parameters.");
    }
  }
);

module.exports = seatRouter;
