const tableRouter = require("express").Router();
const Room = require("../../models/Room");
const Table = require("../../models/Table");
const Seat = require("../../models/Seat");
const auth = require("../auth");
const {
  sendSuccessResponse,
  sendErrorResponse,
} = require("../../utils/network");
const { solveFeatures } = require("../../utils/functions");
const cors = require("cors");

tableRouter.use(cors());
tableRouter.options("*", cors());

// Table creation
tableRouter.post("/tables", auth.required, async (req, res) => {
  if (
    typeof req.body?.table?.position === "undefined" ||
    typeof req.body?.table?.belongs_to === "undefined"
  ) {
    return sendErrorResponse(res, 406, "Please send the correct parameters.");
  }

  // insert room
  const filter = { _id: req.body.table.belongs_to };
  let room = await Room.findOne(filter);

  // TODO: improve by filtering belongs_to length
  if (room) {
    if (inspectPosition(req.body.table.position)) {
      const roomPos = req.body.table.position;
      let table = new Table();

      // room id
      table.belongs_to = room._id;

      // position key
      table.set("position.x", roomPos.x);
      table.set("position.y", roomPos.y);
      table.set("position.w", roomPos.w);
      table.set("position.h", roomPos.h);
      table.set("position.locked", roomPos.locked);

      // feature key (optional)
      let featureList = [];
      if (req.body?.table?.features) {
        featureList = await solveFeatures(req.body.table.features);

        if (featureList.length > 0) {
          table.features = featureList;
        }
      }

      await table.save().then((response) => {
        return sendSuccessResponse(
          res,
          201,
          "Table successfully created!",
          response
        );
      });
    } else {
      return sendErrorResponse(res, 404, "Request parameters are wrong.");
    }
  } else {
    return sendErrorResponse(res, 404, "This room does not exist!");
  }
});

// Get tables by room
tableRouter.get("/tables/:roomId", auth.required, async (req, res) => {
  if (req.params.roomId) {
    const resultSet = await Table.find({ belongs_to: req.params.roomId })
      .lean()
      .then(async (tables) => {
        for (const table of tables) {
          table["totalSeats"] = await Seat.countDocuments({
            belongs_to: table._id,
          });

          table["busySeats"] = await Seat.countDocuments({
            belongs_to: table._id,
            occupied: true,
          });
        }

        return tables;
      });

    if (resultSet) {
      sendSuccessResponse(
        res,
        200,
        `${
          resultSet.length > 0 ? resultSet.length : "No"
        } tables found for this room.`,
        resultSet
      );
    }
  } else {
    return sendErrorResponse(res, 404, "The room ID is invalid!");
  }
});

// Only for features
// Position will be changed by bulk write
tableRouter.options("*", cors());
tableRouter.put(
  "/update-table-features/:tableId",
  auth.required,
  async (req, res) => {
    let update = {};
    let filter = {};

    if (
      typeof req.params.tableId !== "undefined" &&
      typeof req.body.table.features !== "undefined" &&
      req.body.table.features !== [] &&
      Array.isArray(req.body.table.features)
    ) {
      filter = { _id: req.params.tableId };
      await Table.findOneAndUpdate(
        filter,
        { $set: { features: req.body.table.features } },
        { returnDocument: "after" }
      )
        .then((doc) => {
          sendSuccessResponse(
            res,
            200,
            "Table successfully updated",
            doc.toJSON()
          );
        })
        .catch((e) => {
          sendErrorResponse(res, 400, "Something went wrong!");
          console.log(e);
        });
    } else {
      sendErrorResponse(res, 400, "Parameters are invalid");
    }
  }
);

tableRouter.options("*", cors());
tableRouter.put(
  "/update-table-locked/:tableId",
  auth.required,
  async (req, res) => {
    let filter = {};

    if (
      typeof req.params.tableId !== "undefined" &&
      typeof req.body.table.locked !== "undefined"
    ) {
      filter = { _id: req.params.tableId };
      await Table.findOneAndUpdate(
        filter,
        { $set: { "position.locked": req.body.table.locked } },
        { returnDocument: "after" }
      )
        .then((doc) => {
          sendSuccessResponse(
            res,
            200,
            "Table successfully updated",
            doc.toJSON()
          );
        })
        .catch((e) => {
          sendErrorResponse(res, 400, "Something went wrong!");
          console.log(e);
        });
    } else {
      sendErrorResponse(res, 400, "Parameters are invalid");
    }
  }
);

tableRouter.options("*", cors());
tableRouter.put("/bulk-update-tables", auth.required, async (req, res) => {
  if (
    typeof req.body.tables !== "undefined" &&
    Array.isArray(req.body.tables) &&
    req.body.tables !== []
  ) {
    let bulkArray = [];

    req.body.tables.map((table) => {
      const updateBody = {
        updateOne: {
          filter: { _id: table.id },
          update: {
            "position.x": table.position.x,
            "position.y": table.position.y,
            "position.w": table.position.w,
            "position.h": table.position.h,
          },
        },
      };
      bulkArray.push(updateBody);
    });

    if (bulkArray.length > 0) {
      await Table.bulkWrite(bulkArray)
        .then((response) => {
          sendSuccessResponse(
            res,
            200,
            "Tables updated successfully!",
            response
          );
        })
        .catch((e) => {
          console.error(e);
          sendErrorResponse(res, 400);
        });
    }
  }
});

tableRouter.delete("/table/:tableId", auth.required, async (req, res, next) => {
  const table = await Table.findById(req.params.tableId);

  if (table) {
    // TODO: cascade delete sensors
    await Table.findOneAndDelete({ _id: req.params.tableId })
      .then(() => {
        return sendSuccessResponse(res, 200, "Table deleted successfully!");
      })
      .catch((e) => {
        return sendErrorResponse(res, 400);
      });
  } else {
    return sendErrorResponse(res, 404, "This table does not exist!");
  }
});

const inspectPosition = (positionObj) => {
  if (
    typeof positionObj !== "undefined" &&
    typeof positionObj === "object" &&
    positionObj !== null
  ) {
    if (
      positionObj.hasOwnProperty("x") &&
      positionObj.hasOwnProperty("y") &&
      positionObj.hasOwnProperty("w") &&
      positionObj.hasOwnProperty("h") &&
      positionObj.hasOwnProperty("locked")
    ) {
      if (
        Number.isInteger(positionObj.x) &&
        Number.isInteger(positionObj.y) &&
        Number.isInteger(positionObj.w) &&
        Number.isInteger(positionObj.h)
      ) {
        // we're good
        return true;
      }
    }
  }

  return false;
};

module.exports = tableRouter;
