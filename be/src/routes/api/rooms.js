const mongoose = require("mongoose");
const roomRouter = require("express").Router();
const Room = require("../../models/Room");
const User = require("../../models/User");
const auth = require("../auth");
const {
  sendSuccessResponse,
  sendErrorResponse,
} = require("../../utils/network");
const { solveFeatures } = require("../../utils/functions");
const cors = require("cors");

roomRouter.use(cors());
roomRouter.options("*", cors());

// Room creation
roomRouter.post("/rooms", auth.required, async (req, res) => {
  if (
    typeof req.body?.room?.name === "undefined" ||
    typeof req.body?.room?.created_by === "undefined" ||
    typeof req.body?.room?.location === "undefined"
  ) {
    return sendErrorResponse(
      res,
      406,
      "Please enter the room's name and location!"
    );
  }

  const room = new Room();
  // TODO: add check for if created_by is valid
  const userId = new mongoose.mongo.ObjectId(req.body.room.created_by);
  if (!userId) {
    return sendErrorResponse(res, 404, "This user ID is invalid!");
  }
  let userObj = {};

  room.name = req.body.room.name;
  room.location = req.body.room.location;
  room.created_by = req.body.room.created_by;

  // PSA: spent at least 8 hours on this
  User.aggregate(
    [
      { $match: { _id: userId } },
      {
        $lookup: {
          from: "poly-users",
          as: "Users",
          let: { id: "$id" },
          pipeline: [
            {
              $match: {
                $expr: [{ $eq: ["$id", "$$id"] }],
              },
            },
          ],
        },
      },
      {
        $project: {
          _id: 1,
          email: 1,
          name: 1,
          role: 1,
          createdAt: 1,
          updatedAt: 1,
        },
      },
    ],
    async (err, r) => {
      if (err) {
        console.error(err);
        return sendErrorResponse(res, 500);
      } else {
        if (r) {
          let featureList = [];
          if (req.body?.room?.features) {
            featureList = await solveFeatures(req.body.room.features);
          }

          // TODO: db is being queried twice - fix
          await Room.findOneAndUpdate(
            { _id: room._id },
            {
              created_by: r[0]._id,
              name: room.name,
              location: room.location,
              features: featureList,
            },
            {
              new: true,
              upsert: true,
            }
          );

          // const populatedRoom = await populateCreator(room._id);
          // TODO: db is being queried twice - fix
          // Will auto-populate from mongoose-autopopulate
          const populatedRoom = await Room.findOne(
            { _id: room._id },
            "-id"
          ).exec();

          // console.log(populatedRoom.toObject({ virtuals: true }));
          return sendSuccessResponse(
            res,
            201,
            "Room successfully created!",
            populatedRoom
          );
        }
      }
    }
  );

  room.created_by = userObj;
});

// Get all rooms
roomRouter.get("/rooms", auth.required, async (req, res) => {
  const rooms = await Room.find();
  if (!rooms) {
    return sendErrorResponse(res, 401, "There are no registered rooms!");
  }
  return sendSuccessResponse(res, 200, `Found ${rooms.length} rooms!`, rooms);
});

// Get room by ID
roomRouter.get("/room", auth.required, async (req, res, next) => {
  const room = await Room.findById(req.body.room.id).catch(next);
  if (!room) {
    return sendErrorResponse(res, 401, "Room ID is invalid!");
  }
  return sendSuccessResponse(
    res,
    200,
    "Room retrieved successfully",
    room.toJSON()
  );
});

roomRouter.options("*", cors());
// Get rooms by user ID
roomRouter.get("/room-by-user", async (req, res, next) => {
  if (req.body.user.id) {
    const filter = { created_by: req.body.user.id };

    let rooms = await Room.find(filter);

    if (!rooms || rooms.length === 0) {
      return sendErrorResponse(
        res,
        404,
        "There are no rooms associated with this user!"
      );
    }
    return sendSuccessResponse(
      res,
      200,
      `${rooms.length} rooms successfully retrieved`,
      rooms
    );
  }

  sendErrorResponse(res, 400, "Please send the User's ID");
});

// Update room
roomRouter.put("/room", auth.required, async (req, res, next) => {
  const room = Room.findById(req.body.room.id).catch(next);
  if (!room) {
    return sendErrorResponse(res, 401, "Room ID is invalid!");
  }
  // editable fields: name location features
  if (typeof req.body.room.name !== "undefined") {
    room.name = req.body.room.name;
  }
  if (typeof req.body.room.location !== "undefined") {
    room.location = req.body.room.location;
  }
  if (
    typeof req.body.room.features !== "undefined" &&
    req.body.room.features !== [] &&
    Array.isArray(req.body.room.features) === true
  ) {
    room.features = req.body.room.features;
  }

  return room.save().then(() => {
    return sendSuccessResponse(
      res,
      200,
      "Room successfully updated!",
      room.toJSON()
    );
  });
});

// Delete room by ID
roomRouter.delete("/room", auth.required, async (req, res, next) => {
  const room = await Room.findById(req.body.room.id);
  // Only an admin is allowed to delete an entity
  if (room && room.created_by) {
    const user = await User.findById(room.created_by);
    if (user.role === "admin") {
      await Room.findOneAndDelete({ _id: req.body.room.id })
        .then(() => {
          return sendSuccessResponse(res, 200, "Room deleted successfully!");
        })
        .catch((e) => {
          return sendErrorResponse(res, 400, "Something went wrong!");
        });
    } else {
      return sendErrorResponse(
        res,
        403,
        "You don't have the permission to delete!"
      );
    }
  } else {
    return sendErrorResponse(res, 401, "Room ID is invalid!");
  }
});

module.exports = roomRouter;
