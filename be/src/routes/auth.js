require("dotenv").config();

const secret = process.env.API_KEY;
const { expressjwt: expressJwt } = require("express-jwt");

const getTokenFromHeader = (request) => {
  if (
    request.headers.authorization &&
    request.headers.authorization.split(" ")[0] === "Token"
  ) {
    return request.headers.authorization.split(" ")[1];
  }

  return null;
};

const auth = {
  required: expressJwt({
    secret: secret,
    algorithms: ["HS256"],
    userProperty: "payload",
    getToken: getTokenFromHeader,
  }),
  optional: expressJwt({
    secret: secret,
    algorithms: ["HS256"],
    userProperty: "payload",
    credentialsRequired: false,
    getToken: getTokenFromHeader,
  }),
};

module.exports = auth;
