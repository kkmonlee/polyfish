const mongoose = require("mongoose");
const mongooseAutoPopulate = require("mongoose-autopopulate");
const { FEATURE_TYPE_ENUM } = require("../utils/constants");

const { Schema } = mongoose;

const TableSchema = new Schema(
  {
    position: {
      type: Map,
      of: String,
    },
    features: [
      {
        type: String,
        enum: [
          FEATURE_TYPE_ENUM.ethernet,
          FEATURE_TYPE_ENUM.power_plug,
          FEATURE_TYPE_ENUM.group_study,
          FEATURE_TYPE_ENUM.silent_study,
          FEATURE_TYPE_ENUM.video_conf,
          FEATURE_TYPE_ENUM.tv,
          FEATURE_TYPE_ENUM.computer,
        ],
      },
    ],
    belongs_to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Room",
      autopopulate: true,
    },
  },
  {
    timestamps: true,
    collection: "poly-tables",
  }
);

TableSchema.plugin(mongooseAutoPopulate);

module.exports = mongoose.model("Table", TableSchema);
