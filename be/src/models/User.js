require("dotenv").config();
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const secret = process.env.API_KEY;
const Room = require("./Room");

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, "cannot be empty!"],
      match: [/\S+@\S+\.\S+/, "is invalid"],
      index: true,
    },
    name: String,
    password: String,
    salt: String,
    role: {
      type: String,
      enum: ["user", "admin"],
      default: "user",
    },
  },
  { timestamps: true, collection: "poly-users" }
);

UserSchema.plugin(uniqueValidator, { message: "is already taken." });

UserSchema.pre(
  "deleteOne",
  { document: false, query: true },
  async function () {
    const doc = await this.model.findOne(this.getFilter());
    await Room.deleteMany({ created_by: doc._id });
  }
);

// User authentication methods START
UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString("hex");
  this.password = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
};

UserSchema.methods.validPassword = function (password) {
  const hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
  return this.password === hash;
};

UserSchema.methods.generateToken = function () {
  const today = new Date();
  const expiry = new Date(today);
  expiry.setDate(today.getDate() + 60);

  return jwt.sign(
    {
      id: this._id,
      email: this.email,
      expiry: parseInt(expiry.getTime() / 1000),
    },
    secret
  );
};

UserSchema.methods.toJSON = function () {
  return {
    id: this._id,
    email: this.email,
    name: this.name,
    role: this.role,
  };
};

UserSchema.methods.toAdminJSON = function () {
  return {
    id: this._id,
    email: this.email,
    token: this.generateToken(),
    name: this.name,
    role: this.role,
  };
};

// User authentication methods END

module.exports = mongoose.model("User", UserSchema);
