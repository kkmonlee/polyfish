const mongoose = require("mongoose");
const { FEATURE_TYPE_ENUM } = require("../utils/constants");
const mongooseAutoPopulate = require("mongoose-autopopulate");
const Table = require("./Table");

const { Schema } = mongoose;

const RoomSchema = new Schema(
  {
    name: { type: String, required: [true, "cannot be empty!"] },
    location: { type: String, required: [true, "cannot be empty!"] },
    features: [
      {
        type: String,
        enum: [
          FEATURE_TYPE_ENUM.ethernet,
          FEATURE_TYPE_ENUM.power_plug,
          FEATURE_TYPE_ENUM.group_study,
          FEATURE_TYPE_ENUM.silent_study,
          FEATURE_TYPE_ENUM.video_conf,
          FEATURE_TYPE_ENUM.tv,
          FEATURE_TYPE_ENUM.computer,
        ],
      },
    ],
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      autopopulate: { select: "id name email role" },
    },
  },
  {
    timestamps: true,
    collection: "poly-rooms",
  }
);

RoomSchema.pre(
  "findOneAndDelete",
  { document: false, query: true },
  async function () {
    const doc = await this.model.findOne(this.getFilter());
    await Table.deleteMany({ belongs_to: doc._id });
  }
);
RoomSchema.pre(
  "deleteMany",
  { document: false, query: true },
  async function () {
    const doc = await this.model.findOne(this.getFilter());
    await Table.deleteMany({ belongs_to: doc._id });
  }
);

RoomSchema.plugin(mongooseAutoPopulate);

module.exports = mongoose.model("Room", RoomSchema);
