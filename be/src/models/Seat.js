const mongoose = require("mongoose");
const mongooseAutoPopulate = require("mongoose-autopopulate");
const AutoIncrement = require("mongoose-sequence")(mongoose);

const { Schema } = mongoose;

const SeatSchema = new Schema(
  {
    occupied: {
      type: Boolean,
      required: true,
    },
    belongs_to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Table",
      autopopulate: true,
    },
  },
  {
    timestamps: true,
    collection: "poly-seats",
  }
);

SeatSchema.plugin(mongooseAutoPopulate);

module.exports = mongoose.model("Seat", SeatSchema);
