const app = require("./app");

if (!module.parent) {
  app.listen(3000);
  console.log("Express started on port 3000");
}
