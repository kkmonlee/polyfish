const sendErrorResponse = (response, code, message, error) => {
  let errors = null;
  if (error) {
    errors = Object.keys(error.errors).reduce((errors, key) => {
      errors[key] = error.errors[key].message;

      return errors;
    }, {});
  }

  return response.status(code).json({
    error: {
      code: code ? code : 400,
      message: message
        ? message
        : "Something wrong happened! Please try again.",
      details: errors ? errors : {},
    },
  });
};

const sendSuccessResponse = (response, code, message, body) => {
  return response.status(code).json({
    success: {
      code: code ? code : 200,
      message: message ? message : "Operation completed successfully!",
      data: body ? body : {},
    },
  });
};

module.exports = {
  sendErrorResponse,
  sendSuccessResponse,
};
