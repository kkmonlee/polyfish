const FEATURE_TYPE_ENUM = Object.freeze({
  ethernet: "Ethernet",
  power_plug: "Power Plug",
  group_study: "Group Study",
  silent_study: "Silent Study",
  video_conf: "Video Conferencing",
  tv: "Television",
  computer: "Computer",
});

module.exports = { FEATURE_TYPE_ENUM };
