const { FEATURE_TYPE_ENUM } = require("./constants");
const delay = (t) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, t);
  });
};

const solveFeatures = async (featureList) => {
  let featuresToAdd = [];
  // We want feature list to be in an array
  // Something like ["Ethernet", "Television"]
  if (featureList.length > 0 && Array.isArray(featureList)) {
    await featureList.forEach((feature) => {
      // If enum matches request features
      if (Object.values(FEATURE_TYPE_ENUM).includes(feature)) {
        // Add to array
        featuresToAdd.push(feature);
      }
    });

    return featuresToAdd;
  } else {
    return [];
  }
};

module.exports = {
  delay,
  solveFeatures,
};
