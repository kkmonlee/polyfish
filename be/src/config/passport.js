const passport = require("passport");
const LocalStrategy = require("passport-local");
const mongoose = require("mongoose");

const User = require("../models/User");

passport.use(
  "local-login",
  new LocalStrategy(
    {
      usernameField: "user[email]",
      passwordField: "user[password]",
    },
    async (email, password, done) => {
      try {
        const user = await User.findOne({ email: email });
        if (!user) {
          return done(null, false, {
            errors: {
              email: "is invalid",
            },
          });
        }

        const isMatch = await user.validPassword(password);
        if (!isMatch) {
          return done(null, false, {
            errors: {
              password: "is invalid",
            },
          });
        }

        return done(null, user);
      } catch (error) {
        console.error(error);
        return done(error, false);
      }
    }
  )
);
