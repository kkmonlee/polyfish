import styled from "styled-components";
import polyStudent from "../assets/images/AdobeStock_489881997.jpeg";
import {Paper} from "@mui/material";
import Colors from "../utils/colors";

const PolyBackground = styled.div`
  background-color: ${Colors.secondary.sunnyYellow};
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  width: 100vw;
  height: 100vh;
  z-index: -100;
`;

const PolyPaper = styled(Paper)`
  background-size: contain;
  background: url(${polyStudent}) no-repeat left 220px center;
`;

export {PolyBackground, PolyPaper};
