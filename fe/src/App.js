import "./App.css";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import {ThemeProvider} from "@mui/material/styles";
import {MuiTheme} from "./utils/constants";
import React from "react";
import {SnackbarProvider} from "notistack";
import Canvas from "./components/Canvas";
import Error from "./pages/Error";
import {ErrorBoundary} from "react-error-boundary";

function App() {
  return (
    <div>
      <BrowserRouter>
        {/*<PolyAppBar />*/}
        <ThemeProvider theme={MuiTheme}>
          <SnackbarProvider
            autoHideDuration={3000}
            preventDuplicate
            maxSnack={3}
          >
            <Routes>
              <Route
                path="/"
                exact
                element={
                  <ErrorBoundary FallbackComponent={Error}>
                    <Home/>
                  </ErrorBoundary>
                }
              />
              <Route
                path="/dashboard"
                element={
                  <ErrorBoundary FallbackComponent={Error}>
                    <Dashboard/>
                  </ErrorBoundary>
                }
              />
              <Route
                path="/admin"
                element={
                  <ErrorBoundary FallbackComponent={Error}>
                    <Canvas/>
                  </ErrorBoundary>
                }
              />
              <Route path="/error" element={<Error/>}/>
            </Routes>
          </SnackbarProvider>
        </ThemeProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
