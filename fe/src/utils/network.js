import axios from "axios";

function getLocalAccessToken() {
  const userBody = JSON.parse(localStorage.getItem("userBody"));
  if (userBody) {
    return userBody.token;
  }
}

const axiosInstance = axios.create({
  baseURL: "https://api.polyfish.systems",
  // baseURL: "http://localhost:3000",
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
  },
});
// response.data.error.message

axiosInstance.interceptors.request.use(
  (config) => {
    const token = getLocalAccessToken();
    if (token) {
      config.headers["Authorization"] = "Token " + token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;
