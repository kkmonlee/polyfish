import LanguageIcon from '@mui/icons-material/Language';
import TvIcon from '@mui/icons-material/Tv';
import ComputerIcon from '@mui/icons-material/Computer';
import PowerIcon from '@mui/icons-material/Power';
import DoNotDisturbOnTotalSilenceIcon from '@mui/icons-material/DoNotDisturbOnTotalSilence';
import GroupsIcon from '@mui/icons-material/Groups';
import VideoCallIcon from '@mui/icons-material/VideoCall';
import AirlineSeatReclineNormalIcon from '@mui/icons-material/AirlineSeatReclineNormal';

const {FEATURE_TYPE_ENUM} = require('./constants');

// https://stackoverflow.com/questions/46155/how-can-i-validate-an-email-address-in-javascript
export const validateEmail = (email) => {
  return email.match(
    // eslint-disable-next-line
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
};

export const getFeatureColor = (feature) => {
  switch (feature) {
    case FEATURE_TYPE_ENUM.ethernet:
      return 'error';
    case FEATURE_TYPE_ENUM.tv:
      return 'primary';
    case FEATURE_TYPE_ENUM.computer:
      return 'secondary';
    case FEATURE_TYPE_ENUM.power_plug:
      return 'warning';
    case FEATURE_TYPE_ENUM.silent_study:
      return 'info';
    case FEATURE_TYPE_ENUM.group_study:
      return 'success';
    case FEATURE_TYPE_ENUM.video_conf:
      return 'secondary';
    default:
      return 'primary';
  }
};

export const getFeatureIcon = (feature) => {
  switch (feature) {
    case FEATURE_TYPE_ENUM.ethernet:
      return <LanguageIcon/>;
    case FEATURE_TYPE_ENUM.tv:
      return <TvIcon/>;
    case FEATURE_TYPE_ENUM.computer:
      return <ComputerIcon/>;
    case FEATURE_TYPE_ENUM.power_plug:
      return <PowerIcon/>;
    case FEATURE_TYPE_ENUM.silent_study:
      return <DoNotDisturbOnTotalSilenceIcon/>;
    case FEATURE_TYPE_ENUM.group_study:
      return <GroupsIcon/>;
    case FEATURE_TYPE_ENUM.video_conf:
      return <VideoCallIcon/>;
    default:
      return <AirlineSeatReclineNormalIcon/>;
  }
};

// Code from https://mui.com/material-ui/react-avatar/#letter-avatars
export function stringToColor(string) {
  let hash = 0;
  let i;
  
  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }
  
  let color = '#';
  
  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */
  
  return color;
}

export function stringAvatar(name) {
  return {
    sx: {
      bgcolor: stringToColor(name),
    },
    children: `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
  };
}
