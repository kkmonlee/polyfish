import {createTheme} from "@mui/material/styles";
import Colors from "./colors";

export const FEATURE_TYPE_ENUM = Object.freeze({
  ethernet: "Ethernet",
  power_plug: "Power Plug",
  group_study: "Group Study",
  silent_study: "Silent Study",
  video_conf: "Video Conferencing",
  tv: "Television",
  computer: "Computer",
});

export const MuiTheme = createTheme({
  typography: {
    fontFamily: ["Alexandria"],
  },
  palette: {
    type: "light",
    primary: {
      main: Colors.tertiary.mauveRed,
    },
    secondary: {
      main: Colors.secondary.sunnyYellow,
    },
  },
});

export const universities = [
  {label: "University of Bath"},
  {label: "University of Oxford"},
  {label: "University of Cambridge"},
  {label: "Coventry University"},
  {label: "University of Bristol"},
  {label: "Imperial College of London"},
  {label: "University of Southampton"},
];
