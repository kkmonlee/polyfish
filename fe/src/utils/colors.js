// Primary
const navy = "#122632";
// Secondary
const dijonMustard = "#E5A500";
const washedDenim = "#91C1D3";
const sunnyYellow = "#FFB92A";
const strongRed = "#d02f2f";
const mauveRed = "#A43838";
const burntOrange = "#D36000";
const forestGreen = "#228B22";
// Tertiary
const jungle = "#8FB69C";
const peach = "#F5E3D1";
const londonGray = "#F7F7F7";
const elderflowerCordial = "#E5EFEC";
// Graphics
const jaipur = "#FFDEE6";
const rainForest = "#145F58";
const chaiTea = "#F0E0B5";

const Colors = {
  primary: {
    navy,
  },
  secondary: {
    dijonMustard,
    washedDenim,
    burntOrange,
    rainForest,
    sunnyYellow,
    forestGreen,
  },
  tertiary: {
    jungle,
    peach,
    londonGray,
    elderflowerCordial,
    chaiTea,
    strongRed,
    mauveRed,
  },
  graphics: {
    jaipur,
  },
};

export default Colors;
