import {useEffect, useState} from "react";
import NotFound from "../components/NotFound";
import PolyAppBar from "../components/PolyAppBar";
import LinearProgress from "@mui/material/LinearProgress";
import {getRoomByUser} from "../services/room.service";
import {useErrorHandler} from "react-error-boundary";

const Admin = () => {
  const [roomsOwned, setRoomsOwned] = useState([]);
  const [user, setUser] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const [loading, setLoading] = useState(true);
  
  const handleError = useErrorHandler();
  
  useEffect(() => {
    if (localStorage.getItem("userBody")) {
      const userBody = JSON.parse(localStorage.getItem("userBody"));
      setUser(userBody);
      if (userBody.role === "admin") {
        setAuthorized(true);
      }
    }
  }, []);
  
  useEffect(() => {
    if (authorized && user) {
      setLoading(true);
      const rooms = fetchRoomsByUser(user.id);
      if (rooms && rooms.length >= 0) {
        setRoomsOwned(rooms);
      }
    }
  }, [authorized, user]);
  
  const fetchRoomsByUser = async (id) => {
    return await getRoomByUser(id)
      .then(() => {
        setLoading(false);
      })
      .catch((error) => {
        handleError(error);
      });
  };
  
  return (
    <div>
      {authorized ? (
        loading ? (
          <div>
            <PolyAppBar/>
            <div className="flex flex-col justify-center">
              <LinearProgress color="secondary" style={{height: 10}}/>
              <div className="flex justify-center font-alex text-5xl mt-5">
                Loading admin attributes...
              </div>
            </div>
          </div>
        ) : (
          <div>Content</div>
        )
      ) : (
        <NotFound
          backButton
          message="Looks like you're not supposed to be here!"
        />
      )}
    </div>
  );
};

export default Admin;
