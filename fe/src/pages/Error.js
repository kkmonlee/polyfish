import serverError from "../assets/images/500.png";
import PolyButton from "../components/Button";
import {useNavigate} from "react-router-dom";

const Error = () => {
  const navigate = useNavigate();
  
  return (
    <div className="container mx-auto flex flex-col justify-center h-screen">
      <div className="font-alex text-3xl flex justify-center">
        Something really went wrong there...
      </div>
      <div className="flex justify-center">
        <PolyButton
          width={160}
          onClick={() => navigate("/")}
          className="my-4"
          padding="py-2"
        >
          Try again
        </PolyButton>
      </div>
      <div className="flex justify-center">
        <img className="w-1/2" src={serverError} alt="Server error"/>
      </div>
    </div>
  );
};

export default Error;
