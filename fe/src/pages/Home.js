import {PolyBackground} from "../styles/Home";
import {Zoom} from "@mui/material";
import logo from "../assets/polyfish-t-b.png";
import student from "../assets/images/student.png";
import {forwardRef, useEffect, useState} from "react";
import LoginDialog from "../components/LoginDialog";
import RegisterDialog from "../components/RegisterDialog";
import {useNavigate} from "react-router-dom";
import PolyButton from "../components/Button";

const Transition = forwardRef(function Transition(props, ref) {
  return <Zoom direction="up" ref={ref} {...props} />;
});

const Home = () => {
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [registerModalOpen, setRegisterModalOpen] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  
  const navigate = useNavigate();
  
  useEffect(() => {
    if (localStorage.getItem("userBody")) {
      const userBody = JSON.parse(localStorage.getItem("userBody"));
      if (userBody.token) {
        setIsLoggedIn(true);
      }
    }
  }, []);
  
  const handleLoginModalClick = () => {
    setLoginModalOpen(true);
  };
  
  const handleLoginModalClose = () => {
    setLoginModalOpen(false);
  };
  
  return (
    <>
      <PolyBackground>
        <div>
          <img
            className="absolute bottom-0 right-0 h-1/2"
            src={student}
            alt="student"
          />
        </div>
        <div className="w-28 pt-16 ml-20">
          <img className="ml-8" alt="Polyfish Logo" src={logo}/>
        </div>
        <div className="flex flex-col justify-center items-center -mt-6 gap-y-14">
          <div className="font-alex text-7xl font-bold ml-12 text-center">
            Never look for a <br/>{" "}
            <span className="underline underline-offset-[12px] decoration-[12px] decoration-mauve">
              seat
            </span>{" "}
            again<span className="text-mauve">.</span>
          </div>
          <div className="break-words font-alex text-lg text-center -mt-4">
            Polyfish helps you find a vacant seat around <br/>
            campus using Internet of Things.
          </div>
          {isLoggedIn ? (
            <div>
              <PolyButton onClick={() => navigate("/dashboard")}>
                Go to Dashboard
              </PolyButton>
              <div className="break-words font-alex text-md text-center mt-2">
                You're already logged in.
              </div>
            </div>
          ) : (
            <>
              <PolyButton onClick={handleLoginModalClick}>
                Sign in
              </PolyButton>
              <div
                className="-mt-10 cursor-pointer font-alex text-md underline text-center underline-offset-2 hover:text-mauve hover:duration-150"
                onClick={() => setRegisterModalOpen(true)}
              >
                Don't have an account? <br/> Register now!
              </div>
            </>
          )}
          
          {/*<Box*/}
          {/*  sx={{*/}
          {/*    display: "flex",*/}
          {/*    justifyContent: "end",*/}
          {/*    flexWrap: "wrap",*/}
          {/*    "& > :not(style)": {*/}
          {/*      mt: 25,*/}
          {/*      mr: 5,*/}
          {/*      width: 600,*/}
          {/*      height: 500,*/}
          {/*      borderRadius: 4,*/}
          {/*      // backgroundColor: "transparent",*/}
          {/*      // backdropFilter: "blur(100px)",*/}
          {/*    },*/}
          {/*  }}*/}
          {/*>*/}
          {/*  <PolyPaper elevation={24}>*/}
          {/*    <div className="font-alex font-normal text-6xl ml-8 mt-8">*/}
          {/*      Sign in.*/}
          {/*    </div>*/}
          {/*    <div className="mt-14 ml-8">*/}
          {/*      <div className="font-alex text-lg mb-2">Email address</div>*/}
          {/*      <TextField sx={{ width: 300 }} />*/}
          {/*      <div className="font-alex text-lg mt-4 mb-2">Password</div>*/}
          {/*      <TextField sx={{ width: 300 }} />*/}
          {/*    </div>*/}
          {/*    <div className="mt-4 ml-8">*/}
          {/*      <Button sx={{ width: 300 }} variant="contained">*/}
          {/*        Sign in*/}
          {/*      </Button>*/}
          {/*    </div>*/}
          {/*    <div className="font-alex font-light ml-8 mt-12 underline">*/}
          {/*      <a href="#">Don't have an account? Sign up!</a>*/}
          {/*    </div>*/}
          {/*  </PolyPaper>*/}
          {/*</Box>*/}
        </div>
        <LoginDialog
          open={loginModalOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleLoginModalClose}
          isLoggedIn={() => setIsLoggedIn(true)}
        />
        <RegisterDialog
          open={registerModalOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setRegisterModalOpen(false)}
        />
      </PolyBackground>
    </>
  );
};

export default Home;
