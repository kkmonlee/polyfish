import {useEffect, useState} from "react";
import {getAllRooms} from "../services/room.service";
import RoomCard from "../components/RoomCard";
import {Skeleton} from "@mui/material";
import RefreshIcon from "@mui/icons-material/Refresh";
import Tooltip from "@mui/material/Tooltip";
import {useSnackbar} from "notistack";
import RoomDialog from "../components/RoomDialog";
import {getTablesByRoom} from "../services/table.service";

const Rooms = () => {
  const [rooms, setRooms] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedRoom, setSelectedRoom] = useState({
    room: null,
    availability: 0,
  });
  const [showRoomDialog, setShowRoomDialog] = useState(false);
  
  const {enqueueSnackbar} = useSnackbar();
  
  useEffect(() => {
    fetchRooms();
  }, []);
  
  useEffect(() => {
    if (selectedRoom.room) {
      setShowRoomDialog(true);
    }
  }, [selectedRoom]);
  
  useEffect(() => {
    if (rooms) {
      // loading feeling
      setTimeout(() => setLoading(false), 500);
    }
  }, [rooms]);
  
  const handleSelectedRoom = (room, availability) => {
    setSelectedRoom({
      room,
      availability,
    });
  };
  
  const closeRoomDialog = () => {
    setShowRoomDialog(false);
    setSelectedRoom({
      room: null,
      availability: null,
    });
  };
  
  const fetchRooms = () => {
    enqueueSnackbar("Loading rooms...", {
      autoHideDuration: 1000,
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "center",
      },
    });
    
    setLoading(true);
    
    async function fetchData() {
      return await getAllRooms();
    }
    
    fetchData().then(async (res) => {
      const roomsList = res.success.data;
      // setRooms(roomsList);
      
      for (let room of roomsList) {
        room = await getTablesByRoom(room._id).then((res2) => {
          room["tables"] = res2.success.data;
          return room;
        });
      }
      
      setRooms(roomsList);
    });
  };
  
  return (
    <>
      <div className="container py-10 px-5 mx-auto">
        <div className="heading">
          <div className="flex justify-between">
            <div className="font-alex text-4xl">Your rooms</div>
            <div className="cursor-pointer">
              <Tooltip title="Refresh">
                <RefreshIcon onClick={fetchRooms} fontSize="large"/>
              </Tooltip>
            </div>
          </div>
          <div className="font-raleway text-xl">
            {loading ? (
              <Skeleton variant="text" sx={{height: 40, width: 300}}/>
            ) : (
              `${rooms.length} ${rooms.length > 1 ? "rooms" : "room"} available`
            )}
          </div>
        </div>
        
        <div className="rooms grid gap-1 sm:pl-10 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-10">
          {loading
            ? new Array(20)
              .fill(0)
              .map((e, i) => (
                <Skeleton
                  key={i}
                  variant="rectangular"
                  width={250}
                  height={250}
                />
              ))
            : rooms.map((room, i) => (
              <RoomCard
                key={i}
                onDetails={handleSelectedRoom}
                index={++i}
                room={room}
              />
            ))}
        </div>
      </div>
      <RoomDialog
        room={selectedRoom}
        open={showRoomDialog}
        onClose={closeRoomDialog}
      />
    </>
  );
};

export default Rooms;
