import PolyAppBar from "../components/PolyAppBar";
import {Autocomplete, InputAdornment, Skeleton, TextField,} from "@mui/material";
import {universities} from "../utils/constants";
import {useEffect, useState} from "react";
import Rooms from "./Rooms";
import NotFound from "../components/NotFound";
import Search from "../assets/images/search.png";
import SearchIcon from "@mui/icons-material/Search";
import PolyButton from "../components/Button";

const Dashboard = () => {
  const [authorized, setAuthorized] = useState(false);
  const [dropDownInput, setDropDownInput] = useState();
  const [university, setUniversity] = useState();
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(true);
  
  useEffect(() => {
    if (localStorage.getItem("userBody")) {
      const userBody = JSON.parse(localStorage.getItem("userBody"));
      if (userBody.token) {
        setAuthorized(true);
      }
    }
    
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);
  
  useEffect(() => {
    if (university !== "University of Bath" && university) {
      setShowError(true);
    } else {
      setShowError(false);
    }
  }, [university]);
  
  const setAutocomplete = () => {
    if (dropDownInput) {
      setUniversity(dropDownInput.label);
    }
  };
  
  return (
    <>
      {loading ? (
        <div>
          <PolyAppBar/>
          <div className="flex justify-center items-center pb-10 border">
            <div className="flex flex-col gap-8">
              <Skeleton
                className="mt-10"
                variant="rectangular"
                height={50}
                width={400}
              />
              <div className="flex justify-between">
                <Skeleton variant="rectangular" height={50} width={350}/>
                <Skeleton
                  className="ml-6"
                  variant="rectangular"
                  height={50}
                  width={50}
                />
              </div>
            </div>
          </div>
        </div>
      ) : authorized ? (
        <>
          <PolyAppBar/>
          <div className="flex justify-center items-center pb-10 border">
            <div className="flex flex-col gap-8">
              <div className="flex justify-center">
                <h1 className="break-words font-alex font-bold mt-10 text-4xl text-center w-2/3">
                  One platform to help you find the perfect seat anywhere in
                  campus
                </h1>
              </div>
              <div className="flex justify-center mt-5">
                <img className="w-1/4" src={Search} alt="search rooms"/>
              </div>
              <div className="flex justify-center mt-8">
                {/*lg:w-1/3 xl:w-1/3*/}
                <div className="flex justify-between lg:w-2/5 sm:w-1/2">
                  <Autocomplete
                    value={dropDownInput}
                    onChange={(e, newValue) => {
                      setDropDownInput(newValue);
                    }}
                    className="flex-grow mr-6"
                    disablePortal
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        InputProps={{
                          ...params.InputProps,
                          startAdornment: (
                            <InputAdornment position="start">
                              <SearchIcon/>
                            </InputAdornment>
                          ),
                        }}
                        label="University name"
                        placeholder="Enter your university name"
                      />
                    )}
                    options={universities}
                  />
                  <PolyButton onClick={setAutocomplete}>
                    Go!
                  </PolyButton>
                </div>
              </div>
            </div>
          </div>
          {showError ? <NotFound message="Sorry, no rooms found!"/> : null}
          {university === "University of Bath" ? <div><Rooms/></div> : null}
        </>
      ) : (
        <NotFound backButton message="You might be lost..."/>
      )}
    </>
  );
};

export default Dashboard;
