import axiosInstance from "../utils/network";

export const polyLogin = async (email, password) => {
  const data = {
    user: {
      email,
      password,
    },
  };
  
  return await axiosInstance.post("/users/login", data);
};

export const polyLogout = () => {
  if (localStorage.getItem("userBody")) {
    localStorage.removeItem("userBody");
    return true;
  } else {
    return false;
  }
};
