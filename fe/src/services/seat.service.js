import axiosInstance from "../utils/network";

export const getSeatsByTable = async (id) => {
  const response = await axiosInstance.get(`/seats/${id}`);
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
}