import axiosInstance from "../utils/network";

export const createRoom = async (data) => {
  const response = await axiosInstance.post("/rooms", {room: {...data}});
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const getAllRooms = async () => {
  const response = await axiosInstance.get("/rooms");
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const getRoomByID = async (id) => {
  const response = await axiosInstance.get("/room", {data: {id: id}});
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const getRoomByUser = async (userId) => {
  const response = await axiosInstance.get("/room-by-user", {
    data: {user: {id: userId}},
  });
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const updateRoom = async (id, data) => {
  const response = await axiosInstance.put("/room", {
    room: {id: id, ...data},
  });
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const removeRoom = async (id) => {
  const response = await axiosInstance.delete("/room", {data: {id: id}});
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};
