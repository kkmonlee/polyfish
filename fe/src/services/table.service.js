import axiosInstance from "../utils/network";

export const getTablesByRoom = async (id) => {
  const response = await axiosInstance.get(`/tables/${id}`);
  
  if (response) {
    return response.data;
  } else {
    return null;
  }
};