import axiosInstance from "../utils/network";

export const getUser = async (id) => {
  const response = await axiosInstance.get("/user", {data: {id: id}});
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const createUser = async (data) => {
  const response = await axiosInstance.post("/users", {user: {...data}});
  if (response) {
    return response.data;
  } else {
    return null;
  }
};

export const updateUser = async (id, data) => {
  const response = await axiosInstance.put("/user", {
    user: {id: id, ...data},
  });
  if (response) {
    return response.data;
  } else {
    return null;
  }
};