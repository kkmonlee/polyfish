import notFoundImg from "../assets/images/notfound.png";
import {Button} from "@mui/material";
import {useNavigate} from "react-router-dom";

const NotFound = ({message, backButton = false}) => {
  const navigate = useNavigate();
  return (
    <div className="main flex flex-col items-center">
      <h1 className="font-alex text-4xl pt-10 pb-5">
        {message || "This webpage doesn't exist!"}
      </h1>
      {backButton ? (
        <Button onClick={() => navigate("/")} variant="contained">
          Go back
        </Button>
      ) : null}
      
      <img className="w-5/12" src={notFoundImg} alt="Results not found"/>
    </div>
  );
};

export default NotFound;
