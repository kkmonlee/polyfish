import React from "react";
import _ from "lodash";
import {Responsive, WidthProvider} from "react-grid-layout";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import Colors from "../utils/colors";

import {Button, Paper} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import ChairAltIcon from "@mui/icons-material/ChairAlt";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import LockOpenOutlinedIcon from "@mui/icons-material/LockOpenOutlined";
import Tooltip from "@mui/material/Tooltip";

const ResponsiveReactGridLayout = WidthProvider(Responsive);

export default class Canvas extends React.PureComponent {
  static defaultProps = {
    className: "layout",
    breakpoints: {lg: 1200, md: 996, sm: 768, xs: 200, xxs: 0},
    cols: {lg: 12, md: 10, sm: 6, xs: 4, xxs: 2},
    rowHeight: 100,
    verticalCompact: false,
  };
  
  constructor(props) {
    super(props);
    
    this.state = {
      items: this.props.tables.map(function (table, i, list) {
        return {
          i: i.toString(),
          x: i * 2,
          y: 0,
          w: table.position?.w ? Number(table.position.w) : 1,
          h: table.position?.h ? Number(table.position.h) : 1,
          add: i === list.length - 1,
          locked: table.position.locked,
          seats: table.seats,
        };
      }),
      newCounter: 0,
      selectedItem: null,
    };
    
    this.onAddItem = this.onAddItem.bind(this);
    this.onBreakpointChange = this.onBreakpointChange.bind(this);
    this.onLayoutChange = this.onLayoutChange.bind(this);
    this.lockItem = this.lockItem.bind(this);
    this.unlockItem = this.unlockItem.bind(this);
  }
  
  componentDidMount() {
    if (this.props.tables) {
      this.setState((state, props) => {
        return {
          items: props.tables.map(function (table, i, list) {
            return {
              i: i.toString(),
              x: i * 2,
              y: 0,
              w: table.position?.w ? Number(table.position.w) : 1,
              h: table.position?.h ? Number(table.position.h) : 1,
              add: i === list.length - 1,
              locked: Boolean(table.position.locked),
              seats: table.seats,
            };
          }),
          newCounter: 0,
          selectedItem: null,
        };
      });
    }
  }
  
  createElement(el) {
    const removeStyle = {
      position: "absolute",
      right: "2px",
      top: 0,
      cursor: "pointer",
    };
    const lockStyle = {
      position: "absolute",
      left: 0,
      bottom: 0,
      cursor: "pointer",
    };
    const topSeats = el.seats.slice(0, 3);
    const botSeats = el.seats.slice(3, 6);
    
    const i = el.i;
    
    return (
      <Paper
        sx={{
          padding: 1,
          backgroundColor: el.locked
            ? Colors.secondary.burntOrange
            : Colors.secondary.dijonMustard,
        }}
        key={i}
        data-grid={el}
      >
        <div className="relative h-full">
          {this.props.locked ? null : (
            <span
              className="remove z-10"
              style={removeStyle}
              onClick={el.locked ? null : this.onRemoveItem.bind(this, i)}
            >
              <Tooltip
                title={
                  el.locked ? "Cannot delete locked table" : "Delete table"
                }
                placement="right-start"
              >
                <CloseIcon fontSize="small"/>
              </Tooltip>
            </span>
          )}
          
          <div className="flex flex-col justify-between h-full">
            <div className="flex justify-evenly">
              {topSeats.map((seat, i) => (
                <ChairAltIcon
                  fontSize="large"
                  color={seat.occupied ? "success" : ""}
                />
              ))}
            </div>
            <div className="flex justify-evenly">
              {botSeats.map((seat, i) => (
                <ChairAltIcon
                  fontSize="large"
                  color={seat.occupied ? "success" : ""}
                />
              ))}
            </div>
          </div>
          {el.locked ? (
            <span className="select-none h-full w-full cursor-not-allowed">
              <span
                className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 opacity-50 text-4xl">
                {this.props.locked ? "" : "Locked"}
              </span>
            </span>
          ) : null}
          
          {this.props.locked ? null : (
            <span className="lock px-1 py-1" style={lockStyle}>
              {el.locked ? (
                <Tooltip title="Unlock table">
                  <LockOpenOutlinedIcon
                    fontSize="small"
                    onClick={this.unlockItem.bind(this, el)}
                  />
                </Tooltip>
              ) : (
                <Tooltip title="Lock table">
                  <LockOutlinedIcon
                    fontSize="small"
                    onClick={this.lockItem.bind(this, el)}
                  />
                </Tooltip>
              )}
            </span>
          )}
        </div>
      </Paper>
    );
  }
  
  unlockItem(el) {
    let shallowLayout = [...this.state.layout];
    let shallowItems = [...this.state.items];
    _.find(shallowLayout, {i: el.i}).static = false;
    _.find(shallowItems, {i: el.i}).locked = false;
    
    this.setState({
      layout: shallowLayout,
      items: shallowItems,
    });
  }
  
  lockItem(el) {
    let shallowLayout = [...this.state.layout];
    let shallowItems = [...this.state.items];
    _.find(shallowLayout, {i: el.i}).static = true;
    _.find(shallowItems, {i: el.i}).locked = true;
    
    this.setState({
      layout: shallowLayout,
      items: shallowItems,
    });
  }
  
  onAddItem() {
    /*eslint no-console: 0*/
    this.setState({
      // Add a new item. It must have a unique key!
      items: this.state.items.concat({
        i: "n" + this.state.newCounter,
        x: (this.state.items.length * 2) % (this.state.cols || 12),
        y: 0, // puts it at the bottom
        w: 2,
        h: 1,
        locked: false,
      }),
      // Increment the counter to ensure key is always unique.
      newCounter: this.state.newCounter + 1,
    });
  }
  
  // We're using the cols coming back from this to calculate where to add new items.
  onBreakpointChange(breakpoint, cols) {
    this.setState({
      breakpoint: breakpoint,
      cols: cols,
    });
  }
  
  onLayoutChange(layout) {
    this.setState({layout: layout});
  }
  
  onRemoveItem(i) {
    this.setState({items: _.reject(this.state.items, {i: i})});
  }
  
  render() {
    return (
      <div>
        {this.props.locked ? null : (
          <Button variant="contained" sx={{m: 2}} onClick={this.onAddItem}>
            Add Table
          </Button>
        )}
        
        <div className="">
          <ResponsiveReactGridLayout
            style={{minHeight: "24rem"}}
            onLayoutChange={this.onLayoutChange}
            onBreakpointChange={this.onBreakpointChange}
            {...this.props}
          >
            {_.map(this.state.items, (el) => this.createElement(el))}
          </ResponsiveReactGridLayout>
        </div>
      </div>
    );
  }
}
