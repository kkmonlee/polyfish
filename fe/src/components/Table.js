import styled from "styled-components";
import Colors from "../utils/colors";
import {Paper} from "@mui/material";

const StyledTable = styled.div`
  width: 60px;
  height: 30px;
  background-color: ${Colors.secondary.dijonMustard};
`;

const StyledPaper = ({props}) => {
  return (
    <Paper sx={{width: "25%", height: "30%"}} elevation={3}>
      {props.children}
    </Paper>
  );
};

export default function PolyTable(props) {
  const w = 1;
  const h = 3;
  return (
    <div
      style={{
        width: 150 * w,
        height: 30 * h,
      }}
      className="flex bg-mauve flex-col justify-between"
    >
      <div className="flex justify-evenly">
        <div className="w-2 h-2 bg-green-500"/>
        <div className="w-2 h-2 bg-green-500"/>
        <div className="w-2 h-2 bg-green-500"/>
      </div>
      <div className="flex justify-evenly">
        <div className="w-2 h-2 bg-green-500"/>
        <div className="w-2 h-2 bg-green-500"/>
        <div className="w-2 h-2 bg-green-500"/>
      </div>
    </div>
  );
}
//
// const Table = () => {
//   return <div className="bg-teal-600 w-32 h-20 rounded-lg drop-shadow-lg"/>
// }
