import {ThemeProvider} from "@mui/material/styles";
import {
  Box,
  Button,
  Checkbox,
  Container,
  CssBaseline,
  Dialog,
  FormControlLabel,
  Grid,
  LinearProgress,
  TextField,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import polyfishLogo from "../assets/polyfish-t-b.png";
import {useEffect, useState} from "react";
import {validateEmail} from "../utils/functions";
import {polyLogin as login} from "../services/auth.service";
import {MuiTheme} from "../utils/constants";
import {useNavigate} from "react-router-dom";
import {useSnackbar} from "notistack";

const LoginDialog = (props) => {
  const initialDataState = {
    email: "",
    password: "",
  };
  
  const [submitted, setSubmitted] = useState(false);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [isValidPassword, setIsValidPassword] = useState(false);
  const [loginData, setLoginData] = useState(initialDataState);
  const navigate = useNavigate();
  const {enqueueSnackbar} = useSnackbar();
  
  useEffect(() => {
    if (validateEmail(loginData.email)) {
      setIsValidEmail(true);
    } else {
      setIsValidEmail(false);
    }
    
    if (loginData.password.length >= 8) {
      setIsValidPassword(true);
    } else {
      setIsValidPassword(false);
    }
  }, [loginData]);
  
  const cleanup = () => {
    setSubmitted(false);
    setLoginData(initialDataState);
  };
  
  const onBadLogin = (message) => {
    enqueueSnackbar(message, {variant: "error"});
    
    setSubmitted(false);
    // props.onClose();
  };
  
  const onGoodLogin = (message) => {
    enqueueSnackbar(message, {
      variant: "success",
    });
    setSubmitted(false);
    props.isLoggedIn(true);
    props.onClose();
    
    enqueueSnackbar("Redirecting you to the dashboard...", {variant: "info"});
    setTimeout(() => {
      navigate("/dashboard");
    }, 1500);
  };
  
  const closeDialog = () => {
    cleanup();
    props.onClose();
  };
  
  const handleLogin = async (e) => {
    e.preventDefault();
    
    setSubmitted(true);
    try {
      await login(loginData.email, loginData.password)
        .then(async (response) => {
          if (response && response.data) {
            const responseBody = response.data.success || response.data.error;
            if (response.data.success) {
              localStorage.setItem(
                "userBody",
                JSON.stringify(responseBody.data)
              );
              onGoodLogin(responseBody.message || "Success!");
            } else {
              onBadLogin(
                responseBody.message ||
                "Something went terribly wrong, please try again!"
              );
            }
          }
        })
        .catch((error, response) => {
          if (error?.response?.data?.error) {
            onBadLogin(error.response.data.error.message);
          }
        });
    } catch (e) {
      onBadLogin("Something went terribly wrong, please try again!");
    }
  };
  
  return (
    <>
      <Dialog open={props.open} onClose={closeDialog} {...props}>
        <ThemeProvider theme={MuiTheme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <Box sx={{display: "flex", justifyContent: "space-between"}}>
              <Box>
                <h3 className="flex font-alex text-4xl mt-5">Sign In</h3>
              </Box>
              <Box
                sx={{
                  mt: 3,
                  cursor: "pointer",
                }}
              >
                <CloseIcon onClick={closeDialog}/>
              </Box>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Box
                component="img"
                sx={{m: 1, width: 1 / 4}}
                src={polyfishLogo}
              />
              <Box
                component="form"
                onSubmit={handleLogin}
                noValidate
                sx={{px: 2, py: 3, borderRadius: 2}}
              >
                <TextField
                  margin="normal"
                  value={loginData.email}
                  onChange={(e) => {
                    setLoginData({
                      ...loginData,
                      email: e.target.value,
                    });
                  }}
                  error={loginData.email.length > 0 ? !isValidEmail : false}
                  helperText={
                    loginData.email.length > 0 && !isValidEmail
                      ? "Please enter a valid email address"
                      : false
                  }
                  required
                  fullWidth
                  id="email"
                  label="Email address"
                  name="email"
                  autoFocus
                />
                <TextField
                  margin="none"
                  value={loginData.password}
                  onChange={(e) => {
                    setLoginData({
                      ...loginData,
                      password: e.target.value,
                    });
                  }}
                  error={
                    loginData.password.length > 0 ? !isValidPassword : false
                  }
                  helperText={
                    loginData.password.length > 0 && !isValidPassword
                      ? "Please enter a password with at least 8 characters"
                      : false
                  }
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary"/>}
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  disabled={submitted || !(isValidEmail && isValidPassword)}
                  sx={{
                    mt: 3,
                    // backgroundColor: Colors.tertiary.mauveRed,
                    // color: "white",
                  }}
                >
                  <span className="font-alex font-semibold tracking-widest">
                    Sign In
                  </span>
                </Button>
                {submitted ? <LinearProgress/> : null}
                <Grid sx={{mt: 2}} container>
                  <Grid item xs>
                    <div className="underline text-mauve text-sm font-alex cursor-pointer">
                      Forgot password?
                    </div>
                  </Grid>
                  <Grid item>
                    <div className="underline text-mauve text-sm font-alex cursor-pointer">
                      Don't have an account? Sign up!
                    </div>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      </Dialog>
    </>
  );
};

export default LoginDialog;
