import {Chip} from "@mui/material";

const FeaturePills = (props) => {
  return (
    <Chip
      icon={props.icon}
      label={props.label}
      color={props.color}
      size={props.size}
      {...props}
    />
  );
};

export default FeaturePills;