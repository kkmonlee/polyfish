import {Dialog, Skeleton} from "@mui/material";
import {useEffect, useState} from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import FeaturePills from "./FeaturePills";
import CloseIcon from "@mui/icons-material/Close";
import {getFeatureColor, getFeatureIcon} from "../utils/functions";
import Canvas from "./Canvas";
import {getSeatsByTable} from "../services/seat.service";
import {buildStyles, CircularProgressbarWithChildren,} from "react-circular-progressbar";
import Colors from "../utils/colors";
import NoTables from "../assets/images/no-tables.png";

const RoomDialog = ({open, onClose, room}) => {
  const [loading, setLoading] = useState(true);
  const [tables, setTables] = useState([]);
  const [selectedRoom, setSelectedRoom] = useState(null);
  
  useEffect(() => {
    if (tables) {
      fetchSeats().then(() => {
        setLoading(false);
      });
    }
  }, [tables]);
  
  useEffect(() => {
    if (room && room.room) {
      setSelectedRoom(room.room);
      setTables(room.room.tables);
    }
  }, [room]);
  
  const fetchSeats = async () => {
    async function fetchData(id) {
      return await getSeatsByTable(id);
    }
    
    for (const table of tables) {
      table["seats"] = await fetchData(table._id).then((res) => {
        return res.success.data;
      });
    }
  };
  
  return (
    <Dialog fullWidth={true} maxWidth="md" open={open} onClose={onClose}>
      {!loading && selectedRoom ? (
        <div className="p-5 overflow-hidden">
          <div className="flex justify-between">
            <div className="font-alex text-3xl">Room details</div>
            <CloseIcon onClick={onClose} className="cursor-pointer"/>
          </div>
          
          <div className="flex justify-between">
            <div className="w-1/2 mt-4">
              {tables.length > 0 ? (
                <>
                  <span className="font-alex text-md">Available tables:</span>
                  <Canvas
                    tables={tables}
                    locked={true}
                    isDraggable={false}
                    isResizable={false}
                  />
                </>
              ) : (
                <div className="flex flex-col">
                  <span className="font-alex text-lg text-center w-full">
                    No tables found...
                  </span>
                  <img
                    className="w-full -mt-8"
                    src={NoTables}
                    alt="no tables illustration"
                  />
                </div>
              )}
            </div>
            <div className="flex flex-col">
              <div className="font-alex ml-3">Features</div>
              <div className="flex">
                <div className="flex flex-wrap w-4/5 ml-3">
                  {selectedRoom.features.length > 0
                    ? selectedRoom.features.map((f, i) => (
                      <FeaturePills
                        key={i}
                        icon={getFeatureIcon(f)}
                        label={f}
                        sx={{
                          padding: 0.5,
                          mr: 1,
                          mt: 1,
                          paddingBottom: 0.5,
                        }}
                        color={getFeatureColor(f)}
                        size="small"
                      />
                    ))
                    : null}
                </div>
                <div className="flex justify-center w-1/5">
                  <div className="w-3/4 font-alex m-1">
                    <CircularProgressbarWithChildren
                      value={room.availability}
                      strokeWidth={10}
                      styles={buildStyles({
                        strokeLinecap: "butt",
                        pathColor:
                          room.availability >= 60
                            ? Colors.tertiary.strongRed
                            : room.availability >= 40
                              ? Colors.secondary.dijonMustard
                              : Colors.secondary.forestGreen,
                        trailColor: "#ccc",
                      })}
                    >
                      <div
                        style={{fontSize: 13}}
                      >{`${room.availability}%`}</div>
                    </CircularProgressbarWithChildren>
                  </div>
                </div>
              </div>
              <div className="font-alex ml-3 mt-3">Details</div>
              <div className="w-full">
                <TableContainer key={`${selectedRoom._id}-container`}>
                  <Table key={selectedRoom._id} aria-label="room details">
                    <TableHead>
                      <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="right">{selectedRoom.name}</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow key={`${selectedRoom._id}-location-row`}>
                        <TableCell>Location</TableCell>
                        <TableCell align="right">
                          {selectedRoom.location}
                        </TableCell>
                      </TableRow>
                      <TableRow key={`${selectedRoom._id}-created-row`}>
                        <TableCell>Created on</TableCell>
                        <TableCell align="right">
                          {new Date(selectedRoom.createdAt).toDateString()}
                        </TableCell>
                      </TableRow>
                      <TableRow key={`${selectedRoom._id}${Math.random()}`}>
                        <TableCell>Last updated date</TableCell>
                        <TableCell align="right">
                          {new Date(selectedRoom.updatedAt).toDateString()}
                        </TableCell>
                      </TableRow>
                      <TableRow key={`${selectedRoom._id}-updated-row`}>
                        <TableCell>Last updated on</TableCell>
                        <TableCell align="right">
                          {
                            new Date(selectedRoom.updatedAt)
                              .toLocaleString()
                              .split(",")[1]
                          }
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="p-5">
          <Skeleton
            className="mb-4"
            variant="rectangular"
            height={50}
            width={400}
          />
          <div className="flex justify-between">
            <Skeleton variant="rectangular" height={300} width={300}/>
            <div className="flex flex-col">
              <div className="flex flex-wrap mb-3">
                <Skeleton className="mr-2" variant="rounded" width={90}/>
                <Skeleton className="mr-2" variant="rounded" width={90}/>
                <Skeleton className="mr-2" variant="rounded" width={90}/>
                <Skeleton className="mr-2" variant="rounded" width={90}/>
              </div>
              <Skeleton variant="rectangular" height={260} width={390}/>
            </div>
          </div>
        </div>
      )}
    </Dialog>
  );
};

export default RoomDialog;
