import {
  AppBar,
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import polyfishLogo from "../assets/polyfish-t-b.png";
import {useEffect, useState} from "react";
import LoginDialog from "./LoginDialog";
import RegisterDialog from "./RegisterDialog";
import {useNavigate} from "react-router-dom";
import {useSnackbar} from "notistack";
import {polyLogout} from "../services/auth.service";
import {stringAvatar} from "../utils/functions";
import {createTheme} from "@mui/material/styles";

const PolyAppBar = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [registerModalOpen, setRegisterModalOpen] = useState(false);
  const [userRole, setUserRole] = useState("user");
  const [userName, setUserName] = useState();
  const [pages, setPages] = useState(["Dashboard", "Contact"]);
  
  const theme = createTheme({
    typography: {
      fontFamily: ["Alexandria"],
    },
    palette: {
      type: "light",
      primary: {
        main: "#FFF",
      },
      secondary: {
        main: "#000",
      },
    },
  });
  
  const {enqueueSnackbar} = useSnackbar();
  const navigate = useNavigate();
  
  const settings = ["Account", "Dashboard"];
  
  useEffect(() => {
    if (localStorage.getItem("userBody")) {
      const userBody = JSON.parse(localStorage.getItem("userBody"));
      setIsLoggedIn(true);
      setUserRole(userBody.role);
      setUserName(userBody.name);
    }
  }, []);
  
  useEffect(() => {
    if (userRole === "admin") {
      setPages(["Dashboard", "Contact", "Admin Portal"]);
    }
  }, [userRole]);
  
  const userLoggedIn = () => {
    setIsLoggedIn(true);
  };
  
  const userLoggedOut = () => {
    polyLogout();
    
    enqueueSnackbar("You're logged out!", {variant: "success"});
    setIsLoggedIn(false);
    handleCloseUserMenu();
    navigate("/");
  };
  
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  
  const handlePageClick = (page) => {
    handleCloseNavMenu();
    switch (page) {
      case pages[0]:
        navigate("/dashboard");
        break;
      case pages[1]:
        navigate("/contact");
        break;
      case pages[2]:
        navigate("/admin");
        break;
      default:
        navigate("/");
        break;
    }
  };
  
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };
  
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  
  const handleLoginModalClick = () => {
    setLoginModalOpen(true);
    handleCloseUserMenu();
  };
  
  const handleRegisterModalClick = () => {
    setRegisterModalOpen(true);
    handleCloseUserMenu();
  };
  
  const handleRegisterModalClose = () => {
    setRegisterModalOpen(false);
    handleCloseUserMenu();
  };
  
  const handleLoginModalClose = () => {
    setLoginModalOpen(false);
    handleCloseUserMenu();
  };
  
  return (
    <>
      <AppBar theme={theme} position="static">
        <Container maxwidth="xl">
          <Toolbar disableGutters>
            <Box
              onClick={() => navigate("/")}
              sx={{display: {xs: "none", md: "flex"}, height: 50}}
            >
              <img
                className="cursor-pointer"
                alt="Polyfish logo"
                src={polyfishLogo}
              />
            </Box>
            <Box sx={{flexGrow: 1, display: {xs: "flex", md: "none"}}}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon/>
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: {xs: "block", md: "none"},
                }}
              >
                {pages.map((page) => (
                  <MenuItem key={page} onClick={() => handlePageClick(page)}>
                    <div className="font-alex font-normal font-black">{page}</div>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <Box
              sx={{display: {xs: "flex", md: "none"}, mr: 1, flexGrow: 1}}
            >
              <img alt="Polyfish logo" className="h-12" src={polyfishLogo}/>
            </Box>
            <Box sx={{flexGrow: 1, display: {xs: "none", md: "flex"}, justifyContent: {md: "end"}}}>
              {pages.map((page) => (
                <Button
                  key={page}
                  onClick={() => handlePageClick(page)}
                  sx={{my: 1, color: "black", display: "block", mr: 4, fontSize: 16}}
                >
                  {page}
                </Button>
              ))}
            </Box>
            
            <Box sx={{flexGrow: 0}}>
              <Tooltip title="Open settings">
                <IconButton onClick={handleOpenUserMenu} sx={{p: 0}}>
                  <Avatar
                    sx={{width: 50, height: 50, fontFamily: "Alexandria", fontWeight: 800}}
                    {...stringAvatar(userName ? userName : "John Doe")}
                  />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{mt: "10px"}}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'center',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                keepMounted
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {settings.map((setting) => {
                  return (
                    <MenuItem
                      key={setting}
                      onClick={handleCloseUserMenu}
                      disabled={!isLoggedIn}
                    >
                      <Typography textAlign="center">{setting}</Typography>
                    </MenuItem>
                  );
                })}
                {isLoggedIn ? (
                  <MenuItem key="logout" onClick={userLoggedOut}>
                    <Typography textAlign="center">Logout</Typography>
                  </MenuItem>
                ) : (
                  <div>
                    <MenuItem key="login" onClick={handleLoginModalClick}>
                      <Typography textAlign="center">Login</Typography>
                    </MenuItem>
                    <MenuItem key="register" onClick={handleRegisterModalClick}>
                      <Typography textAlign="center">Sign Up</Typography>
                    </MenuItem>
                  </div>
                )}
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <LoginDialog
        open={loginModalOpen}
        onClose={handleLoginModalClose}
        isLoggedIn={userLoggedIn}
      />
      <RegisterDialog
        open={registerModalOpen}
        onClose={handleRegisterModalClose}
      />
    </>
  );
};

export default PolyAppBar;
