import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import {forwardRef} from "react";

const Alert = forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export const PolySnackbar = (props) => {
  return (
    <div>
      <Snackbar
        anchorOrigin={
          props.anchorOrigin
            ? props.anchorOrigin
            : {vertical: "bottom", horizontal: "left"}
        }
        open={props.open}
        autoHideDuration={2000}
        onClose={props.onClose}
      >
        {props.severity ? (
          <Alert
            onClose={props.onClose}
            severity={props.severity}
            sx={{width: "100%"}}
          >
            {props.message ? props.message : null}
          </Alert>
        ) : (
          <Alert onClose={props.onClose} sx={{width: "100%"}}>
            {props.message ? props.message : null}
          </Alert>
        )}
      </Snackbar>
    </div>
  );
};
