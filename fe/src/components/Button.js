const PolyButton = (props) => {
  return (
    <div
      onClick={props.onClick}
      style={{width: props.width}}
      className={`${
        props.className
      } border-solid rounded-sm border-black rounded-none border text-xl text-center font-alex z-10 ${
        props.padding || `px-8 py-3`
      } border-2 cursor-pointer bg-transparent hover:bg-mauve hover:text-slate-200 hover:duration-150`}
    >
      {props.children}
    </div>
  );
};

export default PolyButton;
