import {
  Box,
  Button,
  Checkbox,
  Container,
  CssBaseline,
  Dialog,
  FormControlLabel,
  Grid,
  LinearProgress,
  Link,
  TextField,
} from "@mui/material";
import {ThemeProvider} from "@mui/material/styles";
import CloseIcon from "@mui/icons-material/Close";
import polyfishLogo from "../assets/polyfish-t-b.png";
import {useEffect, useState} from "react";
import {validateEmail} from "../utils/functions";
import {createUser} from "../services/user.service";
import {MuiTheme} from "../utils/constants";
import {useSnackbar} from "notistack";

const initialDataState = {
  name: "",
  email: "",
  password: "",
  cPassword: "",
};

const RegisterDialog = (props) => {
  const [registerData, setRegisterData] = useState(initialDataState);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [isValidPassword, setIsValidPassword] = useState(false);
  const [isValidName, setIsValidName] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  
  const {enqueueSnackbar} = useSnackbar();
  
  useEffect(() => {
    if (validateEmail(registerData.email)) {
      setIsValidEmail(true);
    } else {
      setIsValidEmail(false);
    }
    
    if (
      registerData.password.length > 7 &&
      registerData.cPassword === registerData.password
    ) {
      setIsValidPassword(true);
    } else {
      setIsValidPassword(false);
    }
    
    if (
      registerData.name.length !== 0 &&
      registerData.name.trim().indexOf(" ") !== -1
    ) {
      setIsValidName(true);
    } else {
      setIsValidName(false);
    }
  }, [registerData]);
  
  const cleanup = () => {
    setSubmitted(false);
    setRegisterData(initialDataState);
  };
  
  const onGoodRegister = (message) => {
    enqueueSnackbar(message, {
      variant: "success",
    });
    setSubmitted(false);
    enqueueSnackbar("Time to log in!", {
      variant: "info",
    });
    
    props.onClose();
  };
  
  const onBadRegister = (message) => {
    enqueueSnackbar(message, {
      variant: "error",
    });
    setSubmitted(false);
    props.onClose();
  };
  
  const handleRegister = async (e) => {
    e.preventDefault();
    
    setSubmitted(true);
    try {
      const requestBody = {
        name: registerData.name,
        email: registerData.email,
        password: registerData.password,
      };
      await createUser(requestBody)
        .then(async (response) => {
          if (response) {
            const responseBody = response.success || response.error;
            if (responseBody === response.success) {
              onGoodRegister(responseBody.message || "Success!");
            } else {
              onBadRegister(
                responseBody.message ||
                "Something went terribly wrong, please try again!"
              );
            }
          }
        })
        .catch((error) => {
          if (error?.response?.data?.error) {
            onBadRegister(error.response.data.error.message);
          }
        });
    } catch (error) {
      if (error?.response?.data?.error) {
        onBadRegister(error.response.data.error.message);
      }
    }
  };
  
  const closeDialog = () => {
    cleanup();
    props.onClose();
  };
  return (
    <>
      <Dialog open={props.open} onClose={closeDialog} {...props}>
        <ThemeProvider theme={MuiTheme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <Box sx={{display: "flex", justifyContent: "space-between"}}>
              <Box>
                <h3 className="flex font-alex text-4xl mt-5">Sign Up</h3>
              </Box>
              <Box
                sx={{
                  mt: 3,
                  cursor: "pointer",
                }}
              >
                <CloseIcon onClick={closeDialog}/>
              </Box>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Box
                component="img"
                sx={{m: 1, width: 1 / 4}}
                src={polyfishLogo}
              />
              <Box
                component="form"
                onSubmit={handleRegister}
                noValidate
                sx={{px: 2, py: 3, borderRadius: 2}}
              >
                <TextField
                  margin="dense"
                  value={registerData.name}
                  onChange={(e) => {
                    setRegisterData({
                      ...registerData,
                      name: e.target.value,
                    });
                  }}
                  error={!isValidName && registerData.name.length !== 0}
                  helperText={
                    !isValidName && registerData.name.length !== 0
                      ? "Please enter your full name"
                      : false
                  }
                  required
                  fullWidth
                  id="name"
                  label="Full Name"
                  name="name"
                  autoFocus
                />
                <TextField
                  margin="dense"
                  value={registerData.email}
                  onChange={(e) => {
                    setRegisterData({
                      ...registerData,
                      email: e.target.value,
                    });
                  }}
                  error={registerData.email.length > 0 ? !isValidEmail : false}
                  helperText={
                    registerData.email.length > 0 && !isValidEmail
                      ? "Please enter a valid email address"
                      : false
                  }
                  required
                  fullWidth
                  id="email"
                  label="Email address"
                  name="email"
                />
                <TextField
                  margin="dense"
                  value={registerData.password}
                  onChange={(e) => {
                    setRegisterData({
                      ...registerData,
                      password: e.target.value,
                    });
                  }}
                  error={!isValidPassword && registerData.password.length !== 0}
                  helperText={
                    isValidPassword || registerData.password.length === 0
                      ? false
                      : "Passwords must match and greater than 7 characters."
                  }
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                />
                <TextField
                  margin="dense"
                  value={registerData.cPassword}
                  onChange={(e) => {
                    setRegisterData({
                      ...registerData,
                      cPassword: e.target.value,
                    });
                  }}
                  error={
                    !isValidPassword && registerData.cPassword.length !== 0
                  }
                  helperText={
                    isValidPassword || registerData.cPassword.length === 0
                      ? false
                      : "Passwords must match and greater than 7 characters."
                  }
                  required
                  fullWidth
                  name="password"
                  label="Confirm Password"
                  type="password"
                  id="password"
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary"/>}
                  label="Remember me"
                />
                <Button
                  fullWidth
                  type="submit"
                  variant="contained"
                  disabled={
                    submitted ||
                    !(isValidEmail && isValidPassword && isValidName)
                  }
                  sx={{mt: 3}}
                >
                  Sign Up
                </Button>
                {submitted ? <LinearProgress/> : null}
                <Grid sx={{mt: 2}} container>
                  <Grid item>
                    <Link href="#" variant="body2">
                      <div className="font-alex">
                        {"Already have an account? Sign in!"}
                      </div>
                    </Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      </Dialog>
    </>
  );
};

export default RegisterDialog;
