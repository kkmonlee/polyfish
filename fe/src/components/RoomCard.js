import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";

import ArrowCircleDownOutlinedIcon from "@mui/icons-material/ArrowCircleDownOutlined";
import OutboundOutlinedIcon from "@mui/icons-material/OutboundOutlined";
import ArrowCircleUpOutlinedIcon from "@mui/icons-material/ArrowCircleUpOutlined";
import {getFeatureColor, getFeatureIcon} from "../utils/functions";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import Tooltip from "@mui/material/Tooltip";
import LinearProgress from "@mui/material/LinearProgress";
import FeaturePills from "./FeaturePills";
import {useEffect, useState} from "react";

export default function RoomCard({index, room, onDetails}) {
  const [availability, setAvailability] = useState(0);
  
  useEffect(() => {
    if (room && room.tables) {
      const tables = room.tables;
      if (tables.length > 0) {
        let totalSeats = 0;
        let busySeats = 0;
        
        for (const table of tables) {
          totalSeats += table.totalSeats;
          busySeats += table.busySeats;
        }
        
        const occupancy = (busySeats / totalSeats) * 100;
        setAvailability(occupancy);
      }
    }
  }, [room]);
  
  const handleSelectedRoom = () => {
    if (room) {
      onDetails(room, availability);
    }
  };
  
  return (
    <div>
      <Card
        sx={{
          minWidth: 275,
          maxWidth: 250,
          height: 250,
          marginTop: 1,
        }}
        elevation={3}
      >
        <LinearProgress
          variant="determinate"
          value={availability}
          color={
            availability >= 60
              ? "warning"
              : availability >= 40
                ? "secondary"
                : "success"
          }
          sx={{height: 10}}
        />
        <div className="relative h-full">
          <CardContent>
            <div className="flex justify-between">
              <div className="flex items-center">
                <div className="text-md display-font text-slate-500">{`Room #${index}`}</div>
                <div className="flex items-center ml-1 mt-1">
                  {availability >= 60 ? (
                    <>
                      <ArrowCircleUpOutlinedIcon
                        sx={{fontSize: 16, marginBottom: 0.25}}
                        color="warning"
                      />
                      <div className="font-display text-xs font-semibold ml-0.5 mb-1 text-red-700">
                        Crowded
                      </div>
                    </>
                  ) : availability >= 40 ? (
                    <>
                      <OutboundOutlinedIcon
                        sx={{fontSize: 16, marginBottom: 0.25}}
                        color="secondary"
                      />
                      <div className="font-display text-xs font-semibold ml-0.5 mb-1 text-yellow-700">
                        Moderately busy
                      </div>
                    </>
                  ) : (
                    <>
                      <ArrowCircleDownOutlinedIcon
                        sx={{fontSize: 16, marginBottom: 0.25}}
                        color="success"
                      />
                      <div className="font-display text-xs font-semibold ml-0.5 mb-1 text-green-700">
                        Not busy
                      </div>
                    </>
                  )}
                </div>
              </div>
              <Tooltip title="Visit room">
                <OpenInNewIcon
                  onClick={handleSelectedRoom}
                  className="cursor-pointer"
                  size="small"
                />
              </Tooltip>
            </div>
            <div className="mt-2 font-raleway">
              <div className="flex">
                <div className="text-3xl">{room.name}</div>
              </div>
              <div className="text-lg text-slate-600">{room.location}</div>
              <div className="mt-1">
                {room.features.length > 0
                  ? room.features.map((f, i) => (
                    <FeaturePills
                      key={i}
                      icon={getFeatureIcon(f)}
                      label={f}
                      sx={{padding: 0.5, mr: 1, mt: 1, paddingBottom: 0.5}}
                      color={getFeatureColor(f)}
                      size="small"
                    />
                  ))
                  : null}
              </div>
            </div>
            <div className="text-sm display-font text-slate-400 text-right absolute bottom-4 right-0 mr-2">
              Last updated on{" "}
              {new Date(room.updatedAt).toLocaleString("en-GB", {
                year: "numeric",
                month: "numeric",
                day: "numeric",
              })}
            </div>
          </CardContent>
        </div>
      </Card>
    </div>
  );
}
