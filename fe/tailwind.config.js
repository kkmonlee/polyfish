/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        raleway: ["Raleway"],
        alex: ["Alexandria"],
        neuton: ["Neuton"],
      },
      colors: {
        mauve: "#A43838",
      },
    },
  },
  plugins: [],
};
